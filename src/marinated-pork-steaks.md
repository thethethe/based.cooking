# Marinated Pork Steaks 

![Marinated Pork Steaks](pix/marinated-pork-steaks.webp)

A tasty simple marinade that fits well with pork, probably works fine with other meats as well.

- Prep time: 35 minutes
- Cook time: 10 minutes
- Serves: 4 people

## Ingredients

- 3 tablespoons olive oil 
- 0.7 dl soy sauce (¼ cup) (if it's a less salty soy sauce, you can add a bit more)
- 2 tablespoons of honey
- Juice from 1 lime
- 5 peeled cloves garlic (feel free to add more)
- 4 pork steaks

## Directions

1. Mix olive oil, soy sauce, honey and lime juice until you have an even marinade. Add pepper and crushed garlic.
2. Put the pork steaks to soak in the marinade and leave in room temperature for 30-40 minutes.
3. Preheat your grill/frying pan. Add marinated steaks, grill for 2 minutes, flip, repeat until you've grilled them for 10 minutes total. Optionally you can raise the heat a bit for the last few minutes and pour the remaining marinade over the steaks.
5. Let the steaks rest for 10 minutes before serving.

And just like that you got yourself some tasty marinated pork steaks.

## Contribution

- Ricky Lindén - [website](https://rickylinden.com), [donate](https://rickylinden.com/donate.html)
<<<<<<< HEAD
=======

;tags: pork
>>>>>>> 10ffebf2b48fc3de3dd19731195609b8cc4416a4
